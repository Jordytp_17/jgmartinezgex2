﻿using List.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace List
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainController mc;
        public MainWindow()
        {
            InitializeComponent();
            
        }
        
        protected void SetupController()
        {
            mc = new MainController(this);
            this.SaveButton.Click += new RoutedEventHandler(mc.MainEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(mc.MainEventHandler);
            this.AddItem.Click += new RoutedEventHandler(mc.MainEventHandler);
        }
    }

}
