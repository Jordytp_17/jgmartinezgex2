﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace List.Controllers
{
    public class MainController
    {
        object pwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        
        public MainController (MainWindow window)
        {
            pwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        public void MainEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "AddItem":
                    AddData();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
                case "SaveButton":
                    SaveFile();
                    break;

            }
        }
        private void AddData()
        {

        }
        private void OpenFile()
        {

        }
        private void SaveFile()
        {

        }
        
    }
}
